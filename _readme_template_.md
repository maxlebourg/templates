# Nom de l'outil

## Description
- contexte de l'outil
- objectif de l'outil
- ce que fait l'outil (algorithme/processus, sans décrire la technique)
- données d'entrée et de sortie
- langages utilisés
- librairies utilisées
- logiciels utilisés

## Pré-requis
est-ce qu'il faut installer quelque chose pour utiliser l'outil ? versions (python par exemple), dépendances, librairies, etc

## Guide d'installation
Comment installer cet outil sur le poste utilisateur ? Les étapes, les commandes, les problèmes éventuels en lien avec les pré-requis

## Guide d'utilisation
Comment fonctionne l'outil, les paramètres, ce que l'outil renvoie

## Dépannage
Message d'erreur classique, signaler à l'utilisateur de faire un ticket gitlab si un bug/erreur survient

## Roadmap
Présenter les futurs développements et idées (avec éventuellement des dates/horizons). Signaler aux utilisateurs qu'il faut créer un ticket gitlab pour toute proposition d'amélioration.

## Limites
Identifier ce que l'outil ne fait pas, ne peut pas faire

## Licence
Si l'outil a besoin d'une lience particulière pour utiliser l'outil et ses fonctions (par exemple ArcGis Basic/Standard/Advanced ou autre)

## Contributeurs
email des contributeurs de l'outil

## Contribution gitlab
Coller les lignes de commandes git qui permettent de push/pull le projet



-----
dans chacune des sections, ajouter des liens, photos, vidéos de présentations et d'explication pour clarifier l'outil.